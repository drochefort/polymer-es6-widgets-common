/* global HTMLElement, Polymer, WidgetsCommon, VideoPlayer */
import defaultConfig from '../data/default-config.json';
/**
 * Polymer lifecycle implementation for my-widget custom element
 * see documentation for details: https://www.polymer-project.org/1.0/docs/devguide/registering-elements.html
 */
class VideoWidget extends HTMLElement {
    /**
     * This method acts like the constructor in Polymer context
     * See: https://www.polymer-project.org/1.0/articles/es6.html
     */
    beforeRegister() {
        console.log('BEFORE REGISTER');

        this.is = 'video-widget';

        this.properties = {
            headerTitle: {
                type: String,
                value: 'Video Widget',
                notify: true,
            },
            videoRef: {
                type: String,
                value: 'xplor_7.3_hurricane',
                notify: true,
            },
            widgetsCommon: {
                type: Object,
                value: new WidgetsCommon(),
                readOnly: true,
            },
            videoPlayer: {
                type: Object,
                value: null,
            },
        };
    }

    /**
     * Called when the element has been created,
     * but before property values are set and local DOM is initialized.
     */
    created() {
        console.log('CREATED');
    }

    get emitter() {
        return this.widgetsCommon.emitter;
    }

    /**
     *  Called after property values are set and local DOM is initialized.
     */
    ready() {
        console.log('READY');
        this.widgetsCommon.configData.load((data) => this.onConfigurationLoad(data), defaultConfig);

        this.$$('button.fullscreen').addEventListener('click', () => {
            console.log('FSCALLED');
            this.widgetsCommon.fullscreen.requestFullScreen();

            this.emitter.emit('my-custom-event', 5);
        });

        this.emitter.on('my-custom-event', (n) => console.log('EVENT FIRED', n));
    }


    /**
     * Called when widget configuration data gets loaded
     */
    onConfigurationLoad(data) {
        this.headerTitle = data.headerTitle;
        this.videoRef = data.ref;
        this.videoPlayer = new VideoPlayer(null, { ref: this.videoRef });
        this.resize();
    }

    /**
     * Force the widget iframe to resize to match its content
     */
    resize() {
        this.widgetsCommon.resizeModule.resize();
    }
}

Polymer(VideoWidget);

