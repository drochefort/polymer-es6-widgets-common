# Polymer ES6 Boilerplate #

Boilerplate for building custom Polymer elements with ES6, Sass and modern tooling.

### How do I get set up? ###

```
#!bash

npm install -g bower vulcanize eslint babel grunt-cli
npm install && bower install
npm start
```

### Technology stack

** Automation **

- Node (version 4 or above)
- Bower to manage Polymer dependency
- NPM for all other dependencies
- Grunt 1.0 task runner

**JavaScript**

- ES6 syntax
- ESLint for linting the code against the [AirBnB](https://github.com/airbnb/javascript) Style guide
- Babel for transpiling
- Rollup for module bundling
- Uglify for minification

**CSS**

- Sass syntax
- Stylelint for linting Sass against the stylelint [standard](https://github.com/stylelint/stylelint-config-standard) style guide
- PostCSS for transpiling into CSS
- Minify with cssnano

**HTML**

- HTML5
- Polymer 1.4